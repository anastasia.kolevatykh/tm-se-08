package ru.kolevatykh.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.*;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.repository.*;
import ru.kolevatykh.tm.service.*;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

import java.util.*;

@Getter
@NoArgsConstructor(force = true)
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private void registryCommand(@NotNull final AbstractCommand command) throws Exception {
        @NotNull final String commandName = command.getName();
        @NotNull final String commandShortName = command.getShortName();
        @NotNull final String commandDescription = command.getDescription();

        if (commandName.isEmpty()) {
            throw new Exception("[There's no such command name.]");
        }
        if (commandDescription.isEmpty()) {
            throw new Exception("[There's no such command description.]");
        }
        command.setServiceLocator(this);
        commands.put(commandName, command);

        if (!commandShortName.isEmpty()) {
            commands.put(commandShortName, command);
        }
    }

    private void start() {
        System.out.println("*** Welcome to task manager ***"
                + "\nType \"help\" for details.");
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            command = ConsoleInputUtil.getConsoleInput();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(@NotNull final String command) throws Exception{
        if (command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;

        @Nullable final User user = userService.getCurrentUser();
        final boolean isUser = user != null;

        final boolean isAdminOnly = abstractCommand.isRoleAllowed().contains(RoleType.ADMIN)
                && !abstractCommand.isRoleAllowed().contains(RoleType.USER);

        if (abstractCommand.needAuth() && !isUser) {
            throw new Exception("[Command is not allowed. You need to authorize or register.]");
        }

        if (isUser && isAdminOnly && !user.getRoleType().equals(RoleType.ADMIN)) {
            throw new Exception("[Command is not allowed. You need Admin role for this command.]");
        }
        abstractCommand.execute();
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public void init(@NotNull final Class... classes) {
        try {
            for (@NotNull final Class classItem : classes) {
                registryCommand((AbstractCommand) classItem.newInstance());
            }
            userService.createTestUsers();
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
