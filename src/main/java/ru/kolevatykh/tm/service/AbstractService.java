package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IRepository;
import ru.kolevatykh.tm.entity.AbstractEntity;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> {

    @NotNull
    private IRepository<T> repository;

    AbstractService(@NotNull final IRepository<T> repository) {
        this.setRepository(repository);
    }

    @NotNull
    public List<T> findAll() {
        return repository.findAll();
    }

    @Nullable
    public T findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return repository.findOneById(id);
    }

    public void persist(@Nullable final T entity) {
        if (entity == null) return;
        repository.persist(entity);
    }

    public void merge(@Nullable final T entity) {
        if (entity == null) return;
        repository.merge(entity);
    }

    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.remove(id);
    }

    public void removeAll() {
        repository.removeAll();
    }
}
