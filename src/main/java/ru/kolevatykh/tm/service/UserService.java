package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.api.IUserService;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.api.IUserRepository;
import ru.kolevatykh.tm.enumerate.RoleType;

@Setter
@Getter
@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private IUserRepository userRepository;

    @Nullable
    private User currentUser;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.setUserRepository(userRepository);
    }

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findOneByLogin(login);
    }

    public void createTestUsers() {
        @NotNull final User admin = new User("admin", "pass1", RoleType.ADMIN);
        @NotNull final User user = new User("user", "pass2", RoleType.USER);
        userRepository.persist(admin);
        userRepository.persist(user);
    }
}
