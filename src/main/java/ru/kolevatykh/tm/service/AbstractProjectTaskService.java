package ru.kolevatykh.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.AbstractProjectTaskEntity;
import ru.kolevatykh.tm.api.IProjectTaskRepository;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractProjectTaskService<T extends AbstractProjectTaskEntity> {

    @NotNull
    private IProjectTaskRepository<T> projectTaskRepository;

    AbstractProjectTaskService(@NotNull final IProjectTaskRepository<T> projectTaskRepository) {
        this.setProjectTaskRepository(projectTaskRepository);
    }

    @Nullable
    public List<T> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        List<T> taskList = projectTaskRepository.findAll(userId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    @Nullable
    public T findOneById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) return null;
        return projectTaskRepository.findOneById(userId, id);
    }

    @Nullable
    public T findOneByName(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty() || userId == null || userId.isEmpty()) return null;
        return projectTaskRepository.findOneByName(userId, name);
    }

    public void persist(@Nullable final T entity) {
        if (entity == null) return;
        projectTaskRepository.persist(entity);
    }

    public void merge(@Nullable final T entity) {
        if (entity == null) return;
        projectTaskRepository.merge(entity);
    }

    public void remove(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()
                || userId == null || userId.isEmpty()) return;
        projectTaskRepository.remove(userId, id);
    }

    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectTaskRepository.removeAll(userId);
    }
}
