package ru.kolevatykh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.PasswordHashUtil;

@Setter
@Getter
@NoArgsConstructor
public class User extends AbstractEntity {

    @NonNull
    private String login;

    @NonNull
    private String passwordHash;

    @NonNull
    private RoleType roleType;

    @NonNull
    private boolean auth = false;

    public User(@NotNull final String login,
                @NotNull final String password,
                @NotNull final RoleType roleType) {
        super();
        this.login = login;
        this.passwordHash = PasswordHashUtil.getPasswordHash(password);
        this.roleType = roleType;
    }

    public void setPasswordHash(@NotNull final String password) {
        this.passwordHash = PasswordHashUtil.getPasswordHash(password);
    }

    @Override
    public String toString() {
        return "id: '" + id + '\'' +
                ", login: '" + login + '\'' +
                ", roleType: " + roleType +
                ", isAuth: " + auth;
    }
}
