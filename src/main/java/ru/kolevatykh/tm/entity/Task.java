package ru.kolevatykh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor(force = true)
public class Task extends AbstractProjectTaskEntity {

    @Nullable
    protected String projectId;

    public Task(@Nullable final String name, @Nullable final String description,
                @Nullable final Date startDate, @Nullable final Date endDate) {
        super(name, description, startDate, endDate);
    }

    @Override
    public String toString() {
        return "user id: '" + userId + '\'' +
                ", project id: '" + projectId + '\'' +
                ", id: '" + id + '\'' +
                ", name: '" + name + '\'' +
                ", description: '" + description + '\'' +
                ", startDate: " + startDate +
                ", endDate: " + endDate;
    }
}
