package ru.kolevatykh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor(force = true)
public abstract class AbstractProjectTaskEntity extends AbstractEntity {

    @Nullable
    protected String userId;

    @Nullable
    protected String name;

    @Nullable
    protected String description;

    @Nullable
    protected Date startDate;

    @Nullable
    protected Date endDate;

    AbstractProjectTaskEntity(@Nullable final String name, @Nullable final String description,
                              @Nullable final Date startDate, @Nullable final Date endDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
