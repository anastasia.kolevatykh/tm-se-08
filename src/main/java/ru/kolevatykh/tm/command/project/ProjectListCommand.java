package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pls";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all projects.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[PROJECT LIST]");
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll(userId);

        if (projectList == null) {
            System.out.println("[No projects yet.]");
            return;
        }

        @NotNull final StringBuilder projects = new StringBuilder();
        int i = 0;

        for (@NotNull final Project project : projectList) {
            projects
                    .append(++i)
                    .append(". ")
                    .append(project.toString())
                    .append(System.lineSeparator());
        }

        @NotNull final String projectString = projects.toString();
        System.out.println(projectString);
    }
}
