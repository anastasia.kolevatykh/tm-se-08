package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRemoveCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "pr";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[PROJECT REMOVE]\nEnter project name: ");
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();

        if (name.isEmpty()) {
            System.out.println("[The name can't be empty.]");
            return;
        }

        @Nullable final Project project = serviceLocator.getProjectService().findOneByName(userId, name);

        if (project == null) {
            System.out.println("[The project '" + name + "' does not exist!]");
            return;
        }

        @NotNull final String id = project.getId();

        serviceLocator.getTaskService().removeProjectTasks(userId, id);
        serviceLocator.getProjectService().remove(userId, id);
        System.out.println("[Removed project with tasks.]\n[OK]");
    }
}
