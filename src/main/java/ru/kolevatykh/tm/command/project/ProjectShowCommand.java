package ru.kolevatykh.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

import java.util.ArrayList;
import java.util.List;

public final class ProjectShowCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "project-show";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "psh";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow tasks of selected project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[PROJECT SHOW]\nEnter project name: ");
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();

        if (name.isEmpty()) {
            System.out.println("[The name can't be empty.]");
            return;
        }

        @Nullable final Project project = serviceLocator.getProjectService().findOneByName(userId, name);

        if (project == null) {
            System.out.println("[The project '" + name + "' does not exist!]");
            return;
        }

        @NotNull final String id = project.getId();

        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findTasksByProjectId(userId, id);

        if (taskList != null) {
            System.out.println("[TASK LIST]");

            @NotNull final StringBuilder projectTasks = new StringBuilder();
            int i = 0;

            for (@NotNull final Task task : taskList) {
                if (id.equals(task.getProjectId())) {
                    projectTasks
                            .append(++i)
                            .append(". ")
                            .append(task.toString())
                            .append(System.lineSeparator());
                }
            }

            @NotNull final String taskString = projectTasks.toString();
            System.out.println(taskString);
        } else {
            System.out.println("[No tasks yet.]");
        }
    }
}
