package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tls";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all tasks.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[TASK LIST]");
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);

        if (taskList == null) {
            System.out.println("[No tasks yet.]");
            return;
        }

        @NotNull final StringBuilder tasks = new StringBuilder();
        int i = 0;

        for (@NotNull final Task task : taskList) {
            tasks
                    .append(++i)
                    .append(". ")
                    .append(task.toString())
                    .append(System.lineSeparator());
        }

        @NotNull final String taskString = tasks.toString();
        System.out.println(taskString);
    }
}
