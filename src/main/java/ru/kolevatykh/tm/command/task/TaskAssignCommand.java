package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

import java.util.ArrayList;
import java.util.List;

public final class TaskAssignCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "task-assign";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ta";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tAssign task to project.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[ASSIGN TASK TO PROJECT]\nEnter task name:");
        @NotNull final String taskName = ConsoleInputUtil.getConsoleInput();

        if (taskName.isEmpty()) {
            System.out.println("[The task name can't be empty.]");
            return;
        }

        @Nullable final Task task = serviceLocator.getTaskService().findOneByName(userId, taskName);

        if (task == null) {
            System.out.println("[The task '" + taskName + "' does not exist!]");
            return;
        }

        @NotNull final String taskId = task.getId();

        System.out.println("Enter project name:");
        @NotNull final String projectName = ConsoleInputUtil.getConsoleInput();

        if (projectName.isEmpty()) {
            System.out.println("[The name can't be empty.]");
            return;
        }

        @Nullable final Project project = serviceLocator.getProjectService().findOneByName(userId, projectName);

        if (project == null) {
            System.out.println("[The project '" + projectName + "' does not exist!]");
            return;
        }

        @NotNull final String projectId = project.getId();

        serviceLocator.getTaskService().assignToProject(userId, taskId, projectId);
        System.out.println("[OK]");
    }
}
