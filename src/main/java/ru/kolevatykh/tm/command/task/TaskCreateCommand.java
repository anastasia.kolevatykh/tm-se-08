package ru.kolevatykh.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.util.DateFormatterUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class TaskCreateCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "tcr";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tCreate new task.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        @Nullable final User user = serviceLocator.getUserService().getCurrentUser();
        if (user == null) return;
        @NotNull final String userId = user.getId();

        System.out.println("[TASK CREATE]\nEnter task name: ");
        @NotNull final String name = ConsoleInputUtil.getConsoleInput();

        if (name.isEmpty()) {
            System.out.println("[The name can't be empty.]");
            return;
        }

        System.out.println("Enter task description: ");
        @NotNull final String description = ConsoleInputUtil.getConsoleInput();

        System.out.println("Enter task start date: ");
        @NotNull final String taskStartDate = ConsoleInputUtil.getConsoleInput();
        @Nullable Date startDate = DateFormatterUtil.parseDate(taskStartDate);

        System.out.println("Enter task end date: ");
        @NotNull final String taskEndDate = ConsoleInputUtil.getConsoleInput();
        @Nullable Date endDate = DateFormatterUtil.parseDate(taskEndDate);

        @NotNull final Task task = new Task(name, description, startDate, endDate);
        task.setUserId(userId);

        serviceLocator.getTaskService().persist(task);
        System.out.println("[OK]");
    }
}
