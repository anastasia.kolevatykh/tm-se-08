package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;
import ru.kolevatykh.tm.util.PasswordHashUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserAuthorizeCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "user-authorize";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ua";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sign in.";
    }

    @Override
    public boolean needAuth() {
        return false;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        @Nullable final User currentUser = serviceLocator.getUserService().getCurrentUser();

        if (currentUser != null) {
            System.out.println("[You are authorized under login: " + currentUser.getLogin()
                    + "\nPlease LOGOUT first, in order to authorize under OTHER account.]");
            return;
        }

        System.out.println("[USER AUTHORIZATION]\nEnter your login: ");
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();

        @Nullable final User user = serviceLocator.getUserService().findOneByLogin(login);

        if (user == null) {
            System.out.println("[The login '" + login + "' does not exist. Please, retry or register.]");
            return;
        }

        System.out.println("Enter your password: ");
        @NotNull final String password = ConsoleInputUtil.getConsoleInput();

        if (!user.getPasswordHash().equals(PasswordHashUtil.getPasswordHash(password))) {
            System.out.println("[Wrong password.]");
            return;
        }

        user.setAuth(true);
        serviceLocator.getUserService().setCurrentUser(user);
        System.out.println("[OK]");
    }
}
