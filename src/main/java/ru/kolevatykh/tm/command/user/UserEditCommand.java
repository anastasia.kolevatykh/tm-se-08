package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserEditCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ue";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tUpdate login or password.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[USER EDIT]\nEnter your login for edit: ");
        @NotNull final String login = ConsoleInputUtil.getConsoleInput();

        if (login.isEmpty()) {
            System.out.println("[The name can't be empty.]");
            return;
        }

        @Nullable final User user = serviceLocator.getUserService().findOneByLogin(login);

        if (user == null) {
            System.out.println("[The user '" + login + "' does not exist!]");
            return;
        }

        System.out.println("Enter new login: ");
        @NotNull final String loginNew = ConsoleInputUtil.getConsoleInput();

        if (loginNew.isEmpty()) {
            System.out.println("[The name can't be empty.]");
            return;
        }

        System.out.println("Enter new password: ");
        @NotNull final String passwordNew = ConsoleInputUtil.getConsoleInput();

        if (passwordNew.isEmpty()) {
            System.out.println("[The password can't be empty.]");
            return;
        }

        user.setLogin(loginNew);
        user.setPasswordHash(passwordNew);
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }
}
