package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.List;

public final class UserListCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "user-list";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "uls";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tShow all users.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[USER LIST]");
        @NotNull final List<User> userList = serviceLocator.getUserService().findAll();

        if (userList.isEmpty()) {
            System.out.println("[No users yet.]");
        }

        @NotNull final StringBuilder users = new StringBuilder();
        int i = 0;

        for (@NotNull final User user : userList) {
            users
                    .append(++i)
                    .append(". ")
                    .append(user.toString())
                    .append(System.lineSeparator());
        }

        @NotNull final String userString = users.toString();
        System.out.println(userString);
    }
}
