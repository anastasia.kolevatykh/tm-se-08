package ru.kolevatykh.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.command.AbstractCommand;
import ru.kolevatykh.tm.entity.User;
import ru.kolevatykh.tm.enumerate.RoleType;
import ru.kolevatykh.tm.util.ConsoleInputUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserDeleteCommand extends AbstractCommand {
    @NotNull
    protected final List<RoleType> roleTypes = new ArrayList<>();

    @NotNull
    @Override
    public String getName() {
        return "user-delete";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "ud";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "\tDelete account.";
    }

    @Override
    public boolean needAuth() {
        return true;
    }

    @NotNull
    @Override
    public List<RoleType> isRoleAllowed() {
        roleTypes.add(RoleType.USER);
        roleTypes.add(RoleType.ADMIN);
        return roleTypes;
    }

    @Override
    public void execute() {
        System.out.println("[USER DELETE ACCOUNT]");
        System.out.println("Are you sure you want to delete your account? y/n");
        @NotNull final String answer = ConsoleInputUtil.getConsoleInput();

        if (answer.equals("y")) {
            System.out.println("Enter user login: ");
            @NotNull final String login = ConsoleInputUtil.getConsoleInput();

            if (login.isEmpty()) {
                System.out.println("[The name can't be empty.]");
                return;
            }

            @Nullable final User user = serviceLocator.getUserService().findOneByLogin(login);

            if (user == null) {
                System.out.println("[The user '" + login + "' does not exist!]");
                return;
            }

            @NotNull final String id = user.getId();

            serviceLocator.getTaskService().removeAll(id);
            serviceLocator.getProjectService().removeAll(id);
            serviceLocator.getUserService().remove(id);
            serviceLocator.getUserService().setCurrentUser(null);
            System.out.println("[Deleted user account with projects and tasks.]\n[OK]");
        }
    }
}
