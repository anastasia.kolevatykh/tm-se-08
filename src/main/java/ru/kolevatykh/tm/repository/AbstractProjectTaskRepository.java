package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.AbstractProjectTaskEntity;

import java.util.*;

public abstract class AbstractProjectTaskRepository<T extends AbstractProjectTaskEntity> {
    @NotNull
    final Map<String, T> map = new HashMap<>();

    @NotNull
    public List<T> findAll(@NotNull final String userId) {
        List<T> taskList = new ArrayList<>();
        for (Map.Entry<String, T> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId()))
                taskList.add(entry.getValue());
        }
        return taskList;
    }

    @Nullable
    public T findOneById(@NotNull final String userId, @NotNull final String id) {
        for (Map.Entry<String, T> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId())
                    && entry.getValue().getId().equals(id))
                return entry.getValue();
        }
        return null;
    }

    @Nullable
    public T findOneByName(@NotNull final String userId, @NotNull final String name) {
        for (Map.Entry<String, T> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId())
                    && name.equals(entry.getValue().getName()))
                return entry.getValue();
        }
        return null;
    }

    public void merge(@NotNull final T entity) {
        map.put(entity.getId(), entity);
    }

    public void remove(@NotNull final String userId, @NotNull final String id) {
        for (Iterator<Map.Entry<String, T>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, T> entry = it.next();
            T entity = entry.getValue();
            if (userId.equals(entity.getUserId())
                    && entity.getId().equals(id))
                it.remove();
        }
    }

    public void removeAll(@NotNull final String userId) {
        for (Iterator<Map.Entry<String, T>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, T> entry = it.next();
            if (userId.equals(entry.getValue().getUserId()))
                it.remove();
        }
    }
}
