package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.AbstractEntity;

import java.util.*;

public abstract class AbstractRepository<T extends AbstractEntity> {

    @NotNull
    protected final Map<String, T> map = new HashMap<>();

    @NotNull
    public List<T> findAll() {
        return new ArrayList<>(map.values());
    }

    @Nullable
    public T findOneById(@NotNull final String id) {
        return map.get(id);
    }

    public void persist(@NotNull final T user) {
        map.put(user.getId(), user);
    }

    public void merge(@NotNull final T user) {
        map.put(user.getId(), user);
    }

    public void remove(@NotNull final String id) {
        for (Iterator<Map.Entry<String, T>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, T> entry = it.next();
            T task = entry.getValue();
            if (task.getId().equals(id))
                it.remove();
        }
    }

    public void removeAll() {
        if (map.isEmpty()) return;
        map.clear();
    }
}
