package ru.kolevatykh.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.api.ITaskRepository;
import ru.kolevatykh.tm.entity.Task;

import java.util.*;

public final class TaskRepository extends AbstractProjectTaskRepository<Task> implements ITaskRepository {

    @Override
    public void persist(@NotNull final Task task) {
        map.put(task.getId(), task);
    }

    @Override
    public void removeTasksWithProjectId(@NotNull final String userId) {
        for (Iterator<Map.Entry<String, Task>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> entry = it.next();
            Task task = entry.getValue();
            if (userId.equals(task.getUserId())
                    && task.getProjectId() != null)
                it.remove();
        }
    }

    @Override
    public void removeProjectTasks(@NotNull final String userId,
                                   @NotNull final String projectId) {
        for (Iterator<Map.Entry<String, Task>> it = map.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> entry = it.next();
            Task task = entry.getValue();
            if (userId.equals(task.getUserId())
                    && projectId.equals(task.getProjectId()))
                it.remove();
        }
    }

    @NotNull
    @Override
    public List<Task> findTasksByProjectId(@NotNull final String userId,
                                           @NotNull final String projectId) {
        List<Task> taskList = new ArrayList<>();
        for (Map.Entry<String, Task> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId())
                    && projectId.equals(entry.getValue().getProjectId()))
                taskList.add(entry.getValue());
        }
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> findTasksWithoutProject(@NotNull final String userId) {
        List<Task> taskList = new ArrayList<>();
        for (Map.Entry<String, Task> entry : map.entrySet()) {
            if (userId.equals(entry.getValue().getUserId())
                    && entry.getValue().getProjectId() == null)
                taskList.add(entry.getValue());
        }
        return taskList;
    }

    @Override
    public void assignToProject(@NotNull final String userId,
                                @NotNull final String id,
                                @NotNull final String projectId) {
        if (userId.equals(map.get(id).getUserId()))
            map.get(id).setProjectId(projectId);
    }
}
