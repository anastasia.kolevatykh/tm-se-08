package ru.kolevatykh.tm.enumerate;

import org.jetbrains.annotations.NotNull;

public enum RoleType {
    ADMIN("Admin"),
    USER("User");

    @NotNull private final String roleName;

    RoleType(@NotNull final String roleName) {
        this.roleName = roleName;
    }

    public String displayName(){
        return roleName;
    }
}
