package ru.kolevatykh.tm;

import org.jetbrains.annotations.NotNull;
import ru.kolevatykh.tm.bootstrap.Bootstrap;
import ru.kolevatykh.tm.command.general.AboutCommand;
import ru.kolevatykh.tm.command.general.ExitCommand;
import ru.kolevatykh.tm.command.general.HelpCommand;
import ru.kolevatykh.tm.command.project.*;
import ru.kolevatykh.tm.command.task.*;
import ru.kolevatykh.tm.command.user.*;

public final class Application {

    @NotNull
    private final static Class[] COMMAND_INSTANCES = {
            HelpCommand.class,
            ExitCommand.class,
            AboutCommand.class,

            ProjectCreateCommand.class,
            ProjectListCommand.class,
            ProjectShowCommand.class,
            ProjectUpdateCommand.class,
            ProjectRemoveCommand.class,
            ProjectClearCommand.class,

            TaskCreateCommand.class,
            TaskListCommand.class,
            TaskAssignCommand.class,
            TaskUpdateCommand.class,
            TaskRemoveCommand.class,
            TaskClearCommand.class,

            UserAuthorizeCommand.class,
            UserDeleteCommand.class,
            UserEditCommand.class,
            UserListCommand.class,
            UserLogOutCommand.class,
            UserRegisterCommand.class,
            UserShowCommand.class
    };

    public static void main( String[] args ) {
        @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(COMMAND_INSTANCES);
    }
}
