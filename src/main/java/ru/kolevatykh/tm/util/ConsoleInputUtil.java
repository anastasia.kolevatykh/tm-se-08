package ru.kolevatykh.tm.util;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

@NoArgsConstructor
public class ConsoleInputUtil {

    @NotNull
    private static Scanner consoleInput = new Scanner(System.in);

    @NotNull
    public static String getConsoleInput() {
        return consoleInput.nextLine();
    }
}
