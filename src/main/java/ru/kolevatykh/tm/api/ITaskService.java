package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IProjectTaskService<Task> {
    @Nullable List<Task> findAll(@Nullable String userId);

    @Nullable Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable Task findOneByName(@Nullable String userId, @Nullable String name);

    void persist(@Nullable Task task);

    void merge(@Nullable Task task);

    void remove(@Nullable String userId, @Nullable String id);

    void removeAll(@Nullable String userId);

    void removeTasksWithProjectId(@Nullable String userId);

    void removeProjectTasks(@Nullable String userId, @Nullable String projectId);

    @Nullable List<Task> findTasksByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable List<Task> findTasksWithoutProject(@Nullable String userId);

    void assignToProject(@Nullable String userId, @Nullable String id, @Nullable String projectId);
}
