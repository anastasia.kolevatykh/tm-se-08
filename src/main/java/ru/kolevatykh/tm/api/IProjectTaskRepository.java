package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskRepository<T> {
    @NotNull List<T> findAll(@NotNull String userId);

    @Nullable T findOneById(@NotNull String userId, @NotNull String id);

    @Nullable T findOneByName(@NotNull String userId, @NotNull String name);

    void persist(@NotNull T entity);

    void merge(@NotNull T entity);

    void remove(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);
}
