package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kolevatykh.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IProjectTaskRepository<Project> {
    @NotNull List<Project> findAll(@NotNull String userId);

    @Nullable Project findOneById(@NotNull String userId, @NotNull String id);

    @Nullable Project findOneByName(@NotNull String userId, @NotNull String name);

    void persist(@NotNull Project project);

    void merge(@NotNull Project project);

    void remove(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);
}
