package ru.kolevatykh.tm.api;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService<T> {
    @Nullable List<T> findAll(@Nullable String userId);

    @Nullable T findOneById(@Nullable String userId, @Nullable String id);

    @Nullable T findOneByName(@Nullable String userId, @Nullable String name);

    void persist(@Nullable T entity);

    void merge(@Nullable T entity);

    void remove(@Nullable String userId, @Nullable String id);

    void removeAll(@Nullable String userId);
}
